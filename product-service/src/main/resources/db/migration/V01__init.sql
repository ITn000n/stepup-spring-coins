
create table public.products (id bigserial primary key,account bigint,balance double precision,type character varying(128));

INSERT INTO public.products(account, balance, type)
 VALUES (1135, 145.12, 'Card')
 ,(1222,12.05,	'Card')
 ,(1358,	15.05,	'Card')
 ,(123,	0,	'Card');