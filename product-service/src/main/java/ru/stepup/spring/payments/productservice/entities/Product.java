package ru.stepup.spring.payments.productservice.entities;

import jakarta.persistence.*;

import java.math.BigDecimal;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private Long account;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private ProductType type;

    public Product(Long id, Long account, BigDecimal balance, ProductType type) {
        this.id = id;
        this.account = account;
        this.balance = balance;
        this.type = type;
    }

    public Product(Long id, Long account, BigDecimal balance, String type) {
        this.id = id;
        this.account = account;
        this.balance = balance;
        this.type = ProductType.valueOf(type);
    }

    public Product() {
    }

    public Product(Long account, BigDecimal balance, ProductType type) {
        this.account = account;
        this.balance = balance;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", account=" + account +
                ", balance=" + balance +
                ", type=" + type +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }
}
