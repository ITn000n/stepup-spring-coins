package ru.stepup.spring.payments.productservice.services;

import org.springframework.stereotype.Service;
import ru.stepup.spring.payments.productservice.entities.Product;
import ru.stepup.spring.payments.productservice.entities.ProductType;
import ru.stepup.spring.payments.productservice.repositories.ProductRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {


    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void createProduct(Long account, String type) {
        productRepository.save(new Product(account, BigDecimal.valueOf(Math.random()), ProductType.valueOf(type)));
    }

    public void deleteProduct(Product product) {
        productRepository.delete(product);
    }

    public List<Product> getProductsByAccountNumber(Long account) {
        return productRepository.findAll().stream().filter(product -> product.getAccount().equals(account)).collect(Collectors.toList());
    }

    public Product getProductById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
