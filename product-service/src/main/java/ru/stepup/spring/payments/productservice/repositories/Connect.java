package ru.stepup.spring.payments.productservice.repositories;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class Connect {

    private final Connection connect;

    public Connect() {
        try {
            this.connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/javapro", "postgres", "159*357");
        } catch (SQLException e) {
            throw new RuntimeException(e.getCause());
        }
    }

    public Connection getConnection() {
        return connect;
    }
}
