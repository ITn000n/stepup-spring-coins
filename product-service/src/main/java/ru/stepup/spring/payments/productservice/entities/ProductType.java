package ru.stepup.spring.payments.productservice.entities;

public enum ProductType {
    Card,
    Account;
}
