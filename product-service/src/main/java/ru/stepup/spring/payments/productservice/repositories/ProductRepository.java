package ru.stepup.spring.payments.productservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.stepup.spring.payments.productservice.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
