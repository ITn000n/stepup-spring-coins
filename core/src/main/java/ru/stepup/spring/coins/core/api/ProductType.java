package ru.stepup.spring.coins.core.api;

public enum ProductType {
    Card,
    Account;
}
