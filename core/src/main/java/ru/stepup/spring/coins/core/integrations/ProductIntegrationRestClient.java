package ru.stepup.spring.coins.core.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClient;
import ru.stepup.spring.coins.core.exceptions.IntegrationErrorDto;
import ru.stepup.spring.coins.core.exceptions.IntegrationException;
import ru.stepup.spring.coins.core.integrations.dtos.GetProductDtoRq;
import ru.stepup.spring.coins.core.integrations.dtos.GetProductDtoRs;

public class ProductIntegrationRestClient implements ProductIntegration {
    private final RestClient restClient;

    private static final Logger logger = LoggerFactory.getLogger(ProductIntegrationRestClient.class.getName());

    public ProductIntegrationRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public GetProductDtoRs getProduct(String productId, String userId) {
        try {
            GetProductDtoRq getProductDtoRq = new GetProductDtoRq(
                    productId);
            GetProductDtoRs response = restClient.get()
                    .uri("/" + getProductDtoRq.productId())
                    .header("USERID", userId)
                    .header("Accept", "application/json")
                    .retrieve()
                    .onStatus(status -> status.is4xxClientError() || status.is5xxServerError(), (rt, rs) -> {
                        throw new IntegrationException("Продукт не найден", new IntegrationErrorDto(rs.getStatusCode().toString(), "Продукт не найден"));
                    }).body(GetProductDtoRs.class);
            logger.info("response: {}", response);
            return response;
        } catch (IntegrationException e) {
            logger.info("error body: {}", e.getIntegrationErrorDto());
            return null;
        }
    }
}
