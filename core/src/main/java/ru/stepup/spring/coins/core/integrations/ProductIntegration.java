package ru.stepup.spring.coins.core.integrations;

import ru.stepup.spring.coins.core.integrations.dtos.GetProductDtoRs;

public interface ProductIntegration {
    GetProductDtoRs getProduct(String productId, String userId);
}
