package ru.stepup.spring.coins.core.utils;

import java.math.BigDecimal;

public class Utils {
    public static Boolean firstBigDecimalMoreOrEqualSecond(BigDecimal first, BigDecimal second) {
        return switch (first.compareTo(second)) {
            case 0, 1 -> true;
            default -> false;
        };
    }
}