package ru.stepup.spring.coins.core.integrations.dtos;

import ru.stepup.spring.coins.core.api.ProductType;

import java.math.BigDecimal;

public record GetProductDtoRs(Long id,
                              Long account,
                              BigDecimal balance,
                              ProductType type) {
}
