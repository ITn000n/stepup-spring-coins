package ru.stepup.spring.coins.core.services;

import org.springframework.stereotype.Service;
import ru.stepup.spring.coins.core.api.ExecuteCoinsRequest;
import ru.stepup.spring.coins.core.api.ExecuteCoinsResponse;
import ru.stepup.spring.coins.core.api.GetProductResponse;
import ru.stepup.spring.coins.core.configurations.properties.CoreProperties;
import ru.stepup.spring.coins.core.exceptions.BadRequestException;
import ru.stepup.spring.coins.core.utils.Utils;

@Service
public class CoinsService {
    private final CoreProperties coreProperties;
    private final ExecutorService executorService;
    private final ProductService productService;

    public CoinsService(CoreProperties coreProperties, ExecutorService executorService, ProductService productService) {
        this.coreProperties = coreProperties;
        this.executorService = executorService;
        this.productService = productService;
    }

    public ExecuteCoinsResponse execute(ExecuteCoinsRequest request, String userId) {
        if (coreProperties.getNumbersBlockingEnabled()) {
            if (coreProperties.getBlockedNumbers().contains(request.number())) {
                throw new BadRequestException("Указан заблокированный номер кошелька", "BLOCKED_ACCOUNT_NUMBER");
            }
        }

        if (userId == null) {
            throw new BadRequestException("Не указан пользователь", "NOT_FOUND_USERID");
        }

        GetProductResponse r = productService.getProduct(request, userId);
        if (r == null) {
            throw new BadRequestException("Не найден указанный продукт", "NOT_FOUND_PRODUCT");
        }
        if (!Utils.firstBigDecimalMoreOrEqualSecond(r.balance(), request.price())) {
            throw new BadRequestException("Баланс меньше цены", "LOW_BALANCE");
        }

        ExecuteCoinsResponse response = executorService.execute(request);
        if (response == null) {
            throw new BadRequestException("Платёж не прошел", "BAD_COINS_RESPONSE");
        }

        return response;
    }
}
