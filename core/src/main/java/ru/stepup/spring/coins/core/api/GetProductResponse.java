package ru.stepup.spring.coins.core.api;

import java.math.BigDecimal;

public record GetProductResponse(
        Long id,
        Long account,
        BigDecimal balance,
        ProductType type
) {
}
