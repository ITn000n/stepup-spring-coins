package ru.stepup.spring.coins.core.integrations.dtos;

public record GetProductDtoRq(String productId) {
}
