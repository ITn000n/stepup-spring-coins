package ru.stepup.spring.coins.core.services;

import org.springframework.stereotype.Service;
import ru.stepup.spring.coins.core.api.ExecuteCoinsRequest;
import ru.stepup.spring.coins.core.api.GetProductResponse;
import ru.stepup.spring.coins.core.integrations.ProductIntegration;
import ru.stepup.spring.coins.core.integrations.dtos.GetProductDtoRs;

@Service
public class ProductService {
    private final ProductIntegration productIntegration;

    public ProductService(ProductIntegration productIntegration) {
        this.productIntegration = productIntegration;
    }

    public GetProductResponse getProduct(ExecuteCoinsRequest request, String userId) {
        GetProductDtoRs response = productIntegration.getProduct(request.productId(), userId);
        return response == null ? null : new GetProductResponse(response.id(), response.account(), response.balance(), response.type());
    }
}
